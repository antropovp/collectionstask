package com.epam.practice4.service;

import java.io.BufferedReader;
import java.io.FileReader;

public class DemoService {

    public void demo() throws Exception {

        NumberOfCopies noc = new NumberOfCopies();
        UniqueWords uw = new UniqueWords();

        String text = null;

        try(BufferedReader br = new BufferedReader(new FileReader("text.txt"))) {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            text = sb.toString();
        }

        System.out.println("Number of copies for each word:");
        noc.countCopies(text);
        System.out.println();
        System.out.print("Unique words: ");
        uw.printUnique(text);

    }

}
