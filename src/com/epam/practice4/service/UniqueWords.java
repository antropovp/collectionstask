package com.epam.practice4.service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
//Вывести все уникальные слова из текста. Рекомендуется использовать List<String>.
public class UniqueWords {

    public void printUnique(String text) {

        List<String> list = new ArrayList<>();
        List<String> list2 = new ArrayList<>();

        Pattern p = Pattern.compile("[\\w']+");
        Matcher m = p.matcher(text);

        while (m.find()) {
            list.add(text.substring(m.start(), m.end()));
        }

        for (String word: list) {
            word = word.toLowerCase();
            if (!list2.contains(word)) {
                list2.add(word);
            }
        }

        for (int i = 0; i < list2.size(); i++) {
            if (i < list2.size() - 1) {
                System.out.print(list2.get(i) + ", ");
            }
            else {
                System.out.print(list2.get(i) + "!");
            }
        }
    }

}
