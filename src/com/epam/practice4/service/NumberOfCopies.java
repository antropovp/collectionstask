package com.epam.practice4.service;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
//Подсчитать количество повторений для каждого слова - (принять, что знаки пробела,
//знаки препинания являются разделителями слов). Рекомендуется использовать Map<String, Long>.
public class NumberOfCopies {

    public void countCopies(String text) {

        Map<String, Long> map = new HashMap<>();

        Pattern p = Pattern.compile("[\\w']+");
        Matcher m = p.matcher(text);
        String word;
        while (m.find()) {
            word = text.substring(m.start(), m.end()).toLowerCase();
            if(!map.containsKey(word)){
                map.put(word, (long)1);
            }
            else {
                map.put(word, map.get(word)+1);
            }
        }

        for (Map.Entry<String, Long> entry: map.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }

    }

}
